---
title: About
icon: fas fa-info-circle
order: 1
---

<style>
img {
  border-radius: 15%;
}
</style>

![VirtualHub logo](/assets/about.webp){: .shadow }

> Visit [VirtualHub Blog](https://blog.virtualhub.eu.org){:target="_blank"} to know more about the fascinating history and stories about VirtualHub.
{: .prompt-tip }

Welcome to VirtualHub, the playground of legacy Operating Systems. Here, we are going to install the same operating system in many different Virtual Environments and Emulators.

So, how is this different from [Legacy Installer](https://legacyinstaller.pcriot.com/){:target="_blank"}, our old website? The idea behind this separate website had been roaming in my head for a long time. I was getting frustrated by WordPress but did not want to leave it because it provided many features.

VirtualHub is a static site and it is fast. I first used [Publii](https://getpublii.com/){:target="_blank"} to create and manage it. This enabled me to write and edit tutorials offline and publish it after proper editing. Since it is not hosted on any web server, all the writing and editing part is very fast and efficient unlike WordPress.

But Publii had its own limitations, so I began to search for an alternative. This version of VirtualHub is build using [Jekyll](https://jekyllrb.com/){:target="_blank"}.

Initially VirtualHub was a single website, meant to complement the Legacy Installer website. You can still visit the old website here: <https://old.virtualhub.eu.org>{:target="_blank"}. Now, VirtualHub is a platform of many different websites complementing each other and have no relation with Legacy Installer.

Instead of using Windows to write the tutorials and make videos, I will use different Linux distributions. You do not need to worry about it. I have created a separate website to help you install a good Linux Distribution for beginners,  [Kubuntu](https://kubuntu.org){:target="_blank"}, and also help you install software required to follow VirtualHub tutorials on it: <https://setup.virtualhub.eu.org>{:target="_blank"}. You can keep you Windows installation and dual boot. I will use Kubuntu for all the videos from now on. If you want, you can continue using Windows but the steps may need to be modified for you. For emulators and other software that do not have a Linux version or if the Linux version does not work properly, I will use [WINE](https://www.winehq.org/){:target="_blank"} to run them.

I am also introducing several other websites which will be available soon:

- [VirtualHub Blog](https://blog.virtualhub.eu.org){:target="_blank"} - The story about VirtualHub.
- [VirtualHub Screenshots](https://screenshots.virtualhub.eu.org){:target="_blank"} - Screenshots of old Software.
- [VirtualHub DOSBox-X](https://dosbox-x.virtualhub.eu.org){:target="_blank"} - Installing old software directly on DOSBox-X.
