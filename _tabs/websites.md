---
title: Our other websites
icon: fas fa-info-circle
order: 3
---

> [VirtualHub Setup](https://setup.virtualhub.eu.org){:target="_blank"} - Set up you system for VirtualHub.
{: .prompt-tip }

> [VirtualHub Screenshots](https://screenshots.virtualhub.eu.org){:target="_blank"} - Screenshots of old Software.
{: .prompt-tip }

> [VirtualHub DOSBox-X](https://dosbox-x.virtualhub.eu.org){:target="_blank"} - Installing old software directly on DOSBox-X.
{: .prompt-tip }

> [VirtualHub Blog](https://blog.virtualhub.eu.org){:target="_blank"} - The story about VirtualHub.
{: .prompt-info }

> [VirtualHub Old](https://old.virtualhub.eu.org){:target="_blank"} - Old version of VirtualHub, powered by [Publii](https://getpublii.com/){:target="_blank"}.
{: .prompt-danger }
